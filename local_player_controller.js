/* Copyright (C) 2018  Tyler Zachary Powell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/> */

function toggle_hotkeys(){
    toggle_hotkeys.ok_to_use_hotkeys = !(toggle_hotkeys.ok_to_use_hotkeys);
}

//returns the time in seconds from either start or end group
function get_time_from_display(groupID){
    var group = "." + groupID;    
    var minutes = $(group + ".minutes").val();
    var seconds = $(group + ".seconds").val();
    return 60 * Number(minutes) + Number(seconds);
}

//pushes the time to the display of either start or end group
function push_time_to_display(groupID, time){
    var group = "." + groupID;
    var seconds = time % 60;        
    var minutes = (time - seconds) / 60;
    $(group + ".seconds").prop("value", seconds);
    $(group + ".minutes").prop("value", minutes);
}

//sets the current time of the song
function set_time(seconds){
    $("#song").prop("currentTime", seconds);
}

//sets a stop timer
function set_stop_timer(seconds){
    window.clearTimeout(set_stop_timer.timerID);
    var delay = (seconds * 1000) / song.playbackRate;
    set_stop_timer.timerID = window.setTimeout(wrapper, delay);
}
    
function wrapper() {
    $("#song")[0].pause();
}
   
//starts playing the song after a delay
function start_in(seconds) {
    window.clearTimeout(start_in.timerID);
    start_in.timerID = window.setTimeout(wrapperTwo, 1000 * seconds);
}

function wrapperTwo(){
    song.play();
}

//gets the difference between start and end in seconds
function get_interval(){    
    var end = (parseFloat(get_time_from_display("end")));
    var start = (parseFloat(get_time_from_display("start")));
    var rval = end - start;
    return rval;
}

//advances song to next section
function next_interval(){
    var interval = get_interval();
    var endtime = get_time_from_display("end");
    var starttime = get_time_from_display("start");
    push_time_to_display("start", endtime);
    push_time_to_display("end", interval + endtime); 
    play();
}

//check to see if can use hotkeys. could use redesign
function toggle_end(hotkey_used){
    var the_checkbox = $("#use-end-time");
    var ending_inputs = $(".interval-controls");
    if(hotkey_used){   
        the_checkbox.prop("checked", !(the_checkbox.prop("checked")));
    }
    if(the_checkbox.prop("checked") == true){
        $.each(ending_inputs, function(){
            $(this).removeAttr("disabled");
        });
    }
    else{
        $.each(ending_inputs, function(index){
            $(this).attr("disabled", true);
        });
    }
}

//sets up the song, then plays, only happens from front end click
function play(){
    song.pause();
    //set start time based on display
    set_time(get_time_from_display("start"));        
    var wait_before_play = parseFloat(document.getElementById("wait-selector").value);    
    //set up end time if needed
    if(document.getElementById("use-end-time").checked == true){
        set_stop_timer(get_interval() + wait_before_play);
    }
    start_in(wait_before_play);   
}

//try ready again if this doesn't work
$(document).ready(function() { 
    $("#stop").click(function(){
        $("#song")[0].pause();
    });
    $("#play").click(play);
    $("#use-end-time").click(function(){
        toggle_end(false);
    });
    $("#speed-selector").change(function(){
            $("#song").prop("playbackRate", $("#speed-selector").val() / 100);
    });
    $("#next-interval").click(next_interval);
    
    $("#the_file").change(function(){
      var chosen_file = document.getElementById('the_file').files[0];
      var reader = new FileReader();
     
      reader.addEventListener("load", function () {
        song.src = reader.result;
        is_ready.innerHTML = "ready";
        }, false);
     
      reader.readAsDataURL(chosen_file);
    });

    $(".text-input").on("focus", function(){
        toggle_hotkeys();
    });

    $(".text-input").on("blur", function(){
        toggle_hotkeys();
    });
    $(window).on("keydown", function(event){
        if(toggle_hotkeys.ok_to_use_hotkeys){ return; }
        switch(event.key) {
            case "p":
                play();
            break;
            case "s":
                wrapper();
            break;
            case "n":
                next_interval();
            break;
            case "e":
                toggle_end(true);
            break;
        }
    });
});