//The timestamp, in seconds, where the video starts playing. 
var startAtTime = 135;

//The timestamp, in seconds, where the video starts playing.
var stopAtTime = 136;

//speed at which video plays. 0.7 = 70% speed, 1.0 = normal speed. etc.
var speed = 0.65;


// The rest of the script.

// clears the timer that pauses the video.
if(oldTimeout){ window.clearTimeout(oldTimeout); }

//calculate interval. compensate for playback rate and convert to milliseconds.
var interval = ((stopAtTime + 1 - startAtTime) * (1/speed)) * 1000;
//get the video
var vid = document.getElementsByTagName("video")[0];
vid.currentTime = startAtTime;
vid.playbackRate = speed;
vid.play();
var oldTimeout = window.setTimeout(function(){ vid.pause(); },interval);